// const numbers = [5, 2, 9, 8, 10, 11, -3, 0, 22, 4, -1, 5, 2];

// // // 1. Реализовать перебор массива используя 
// // //     -for
// // //     -forEach
// // //     -map
// // // результатом должен быть новый массив, все числа которого на 3 больше исходных

// // //      FOR
// const res = [];
// for (let i = 0, len = numbers.length; i < len; i++) {
//     res.push(numbers[i] + 3)
// };
// console.log(res);
// //
// // //    forEach
// const sum = [];
// numbers.forEach((value, i) => sum.push(numbers[i] + 3));
// console.log(sum);
// //
// // //    MAP
// const summ = numbers.map((value) => value + 3);
// console.log(summ);
// // // ============================================================================
// // // 2. Получить массив чисел больше или равных 10
// // //     -forEach
// // //     -filter
// // // результатом должен быть новый массив

// // //   FILTER
// const filtered = numbers.filter(value => value >= 10);

// console.log(filtered);

// // //   FOREACH
// const numbers1 = [];

// numbers.forEach(value => {
//     if (value >= 10) {
//         numbers1.push(value);
//     }
// });

// console.log(numbers1);

// // // 3. Проверить массив все ли числа в нем больше нуля (хоть одно меньше нуля). 
// // //    Результаты сохранить в isAllPositive, hasNegative соответственно
// // //     -some
// // //     -every

// const isAllPositive = numbers.every(element => element > 0);
// console.log(isAllPositive);

// const hasNegative = numbers.some(element => element < 0);
// console.log(hasNegative);


// // // 4. Посчитать сумму всех элементов массива
// // //     -for
// // //     -forEach
// // //     -reduce

// function arraySum(array) {
//     var sum = 0;
//     for (var i = 0; i < array.length; i++) {
//         sum += array[i];
//     }
//     return sum
// }
// console.log(arraySum(numbers));

// const a = result1();

// function result1() {
//     let sum = 0;
//     numbers.forEach(value => sum += value);
//     return sum;
// }
// console.log(a);



// var summArr = numbers.reduce((accum, currVal) => accum + currVal, 0);

// console.log(summArr);


// // // 5. Отсортировать массив по убыванию
// // //     -sort
// // // результатом должен быть новый массив

// const res1 = [...numbers].sort((a, b) => b - a);
// console.log(res1);



// // // 6. Найти максимальный элемент массива
// // //     -for
// // //     -forEach
// // //     -Math.max
// // //     -reduce

// var maxItem = [];
// for (let i = 0; i < numbers.length; i++) {
//     if (numbers[i] > numbers[0]) {
//         maxItem[0] = numbers[i];
//     }
// }
// console.log(maxItem[0]);

// let maxArra = 0;
// numbers.forEach(function (value) {
//     if (maxArra < value)
//         maxArra = value;
// });
// console.log(maxArra);

// const max = Math.max(...numbers);
// console.log(max);

// const maxArr = numbers.reduce((acc, val) => Math.max(acc, val), 0);

// console.log(maxArr);

// // // 7. Найти индекс элемента 10
// // //     -forEach
// // //     -indexOf

// let elementTen;
// numbers.forEach((val, index) => {
//     if (val === 10)
//         return elementTen = index;
// })
// console.log(elementTen);

// const x = numbers.indexOf(10);
// console.log(x);

// // // 8. Найти элемент -3 и заменить его на число 100
// // //     -map
// // //     -indexOf + splice

// // const elem = numbers.map(function (val) {
// //     return val == -3 ? 100 : val;
// // });
// // console.log(elem);

// // const replacedItem = numbers.splice(numbers.indexOf(-3), 1, 100)

// // console.log(numbers);

// // // 9. Заменить все отрицательные числа на их абсолютные величины (модуль от числа)
// // //     -forEach + splice + Math.abs
// // //     -reduce + Math.abs

// // const remove = numbers;
// // numbers.forEach(function (value) {
// //     if (value < 0)
// //         remove = value;

// //     remove.splice(10, 1, Math.abs(-1));

// //     return remove;
// // });
// // console.log(remove);

// // const negArr = numbers.reduce((acc, value) => {
// //     if (value < 0) {
// //         acc.push(value);
// //         return Math.abs(value)
// //     }
// //     return acc;

// // }, []);

// // console.log(negArr);

// // // 10. Найти в исходном массиве минимальное число,
// // //     создать массив из 10 чисел где стартовым числом будет минимальное найденное из предыдущего шага,
// // //     а каждое последующее на 5 больше предыдущего,
// // //     извлечь только четные числа,
// // //     найти сумму всех элементов полученного массива

// const minNumber = Math.min(...numbers);
// const newArray = [minNumber];

// for (i = 1; i < 10; i++) {
//     newArray.push(newArray[i - 1] + 5);
// };

// // const evenNumb = newArray.filter((value) => {
// //     if (value % 2 == 0) {
// //         return value;
// //     }
// // });

// const evenNumb = [];
// newArray.forEach((value) => {
//     if (value % 2 == 0) {
//         evenNumb.push(value);
//     }
// });

// const sumArray = evenNumb.reduce((total, value) => total + value, 0);

// console.log(minNumber);
// console.log(newArray);
// console.log(evenNumb);
// console.log(sumArray);
// //

// Функция принимает 2 массива.
// Возвращает новый массив, который состоит только из тех элементов,
// которые встретились в одном массиве, но не встретились в другом

function arrayDiff(arrOne, arrTwo) {
    var result = [];
    for (var i = 0; i < arrOne.length; i++) {
        if (arrTwo.indexOf(arrOne[i]) === -1) {
            result.push(arrOne[i]);
        }
    }
    for (i = 0; i < arrTwo.length; i++) {
        if (arrOne.indexOf(arrTwo[i]) === -1) {
            result.push(arrTwo[i]);
        }
    }
    return result;
}
console.log(arrayDiff([1, 2, 3], [1, 2, 4]));         //-> [3, 4]
console.log(arrayDiff([1, 3, 3, 4], [1, 3, '4']));   // -> [4, '4']

// Функция принимает 2 массива, и возвращает массив объединенных значений,
// без дублирования

function union(arrayOne, arrayTwo) {
    return arrayOne.concat(arrayTwo).reduce(function (prev, cur) {
        if (prev.indexOf(cur) === -1) prev.push(cur);
        return prev;
    }, []);
}
console.log(union([5, 1, 2, 3, 3], [4, 3, 2]));      // -> [5, 1, 2, 3, 4]
console.log(union([5, 1, 3, 3, 4], [1, 3, 4]));     // -> [5, 1, 3, 4]

// Реализовать функцию createGenerator.
// При каждом вызове метода .next() происходит возврат следующего значения из массива
// Когда все элементы будут возвращены,
// следующие вызовы метода .next() должны возвращать строку 'Complete!'

function* createGenerator(array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] > 0) {
            yield array[i];
        }
    }
    return 'Complete!';
}

const generator = createGenerator([1, '6', 3, 2]);

console.log(generator.next()); // --> 1
console.log(generator.next()); // --> '6'
console.log(generator.next()); // --> 3
console.log(generator.next()); // --> 2
console.log(generator.next()); // --> 'Complete!'

// 1. Отсортировать по возрасту
// 2. Получить массив имен
// 3. Отфильтровать старше 25
// 4. Создать новый массив объектов где у каждого пользователя появится поле rate: age + index * 3

const users = [
    {
        id: 0,
        name: 'Nikita',
        age: 27
    },
    {
        id: 1,
        name: 'Dima',
        age: 33
    },
    {
        id: 2,
        name: 'Sergey',
        age: 28
    },
    {
        id: 3,
        name: 'Artur',
        age: 21
    }
];
// 1
users.sort((a, b) => {
    return a.age - b.age;
});

users.forEach((value) => {
    console.log(`${value.name} ` + `${value.age}`)
})
// 2

const names = users.map((value) => value.name);
console.log(names);

// 3
const moreThan25 = users.filter((value) => value.age > 25);
console.log(moreThan25);

// 4 

const result = users.map(function (value, i) {
    var addRate = Object.assign({}, value);
    addRate.rate = value.age + [i] * 3;
    return addRate;
})

console.log(result)



// сделать преобразование объекта к примитиву
// const obj = {
//     id: 0,
//     name: 'Obj-name',
//
//     toString() {
//         return this.name;
//     },
//     valueOf() {
//         return this.id;
//     }
// };
//
// console.log(`Name:` + `${obj}`); 		// Name: Obj-name
// console.log(+obj);            		// 0
// console.log(obj + 10);        		// 10
//
//
// Вернуть объект с полями x,y,и колбэком, который ты туда передаёшь
// const factory = (xValue, yValue, funcSumName) => {

//     const someObj = {
//         x: xValue,
//         y: yValue,
//         myFunc() {
//             return this.x + this.y
//         }
//     }
//     return someObj
// }
// const obj = factory(12, 23, 'myFunc');

// console.log(obj.x, obj.y, obj.myFunc()); // 12, 23, 35


// Напишите функцию filterRange(arr, a, b), которая принимает массив arr,
// ищет в нём элементы между a и b и отдаёт массив этих элементов.
// 
//
// function filterRange(arr, a, b) {
//     return arr.filter(value => a <= value && value <= b);
// }

// let arr = [5, 3, 8, 1];

// let filtered = filterRange(arr, 1, 4);

// console.log(filtered);  // 3,1
//
//
// Работа со строками, преобразование в camelCase
//
// function camelize(str) {
//     return str
//         .split('-')
//         .map((value, index) => index == 0 ? value : value[0].toUpperCase() + value.slice(1))
//         .join('');
// }

// camelize("background-color") == 'backgroundColor';
// console.log(camelize("background-color"))
// camelize("list-style-image") == 'listStyleImage';
// console.log(camelize("list-style-image"))
// camelize("-webkit-transition") == 'WebkitTransition';
// console.log(camelize("-webkit-transition"))